package com.hkt.client.rest;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.net.URLEncoder;

import org.springframework.web.client.RestTemplate;

import com.hkt.client.rest.service.HKTFile;
import com.hkt.module.core.rest.Request;
import com.hkt.module.core.rest.Response;
import com.hkt.util.json.JSONSerializer;

public class RESTClient {
	private RestTemplate	restTemplate;
	private String				restUrl;

	public RESTClient() {
		loadIP();

	}

	public RESTClient(String a) {

	}
	
	public boolean runSystemCommand(String command) {
		boolean flag = false;
		String OS = System.getProperty("os.name").toUpperCase();
		try {

			Process p = Runtime.getRuntime().exec("ping " + command);
			BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String s = "";
			// reading output stream of the command
			while ((s = inputStream.readLine()) != null) {
				if (OS.contains("NUX")) {
				if(s.indexOf("ttl=")>0){
					return true;
				}
			}else {
				try {
					if (Double.parseDouble(s.substring(s.indexOf("TTL=") + 4, (s.length()))) <= 255) {
						flag = true;
					}
				} catch (Exception e) {
				}
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}

	private void loadIP() {
		
			try {
				String str = readData();
				restUrl = str;
			} catch (Exception e1) {
				restUrl = "http://localhost:7080/rest";
			}
		
	}

	private String readDataOnline() {
		try {
			FileInputStream fi = new FileInputStream(HKTFile.getFileFromFolder(HKTFile.database, "online"));
			ObjectInputStream of = new ObjectInputStream(fi);
			String str = of.readObject().toString();
			of.close();
			return str;
		} catch (Exception e) {
			return "";
		}
	}


	

	private String readData() {
		try {
			FileInputStream fi = new FileInputStream(HKTFile.getFileFromFolder(HKTFile.database, "rest"));
			ObjectInputStream of = new ObjectInputStream(fi);
			String str = of.readObject().toString();
			of.close();
			return str;
		} catch (Exception e) {
			return "http://localhost:7080/rest";
		}
	}

	public RestTemplate getRestTemplate() {
		return this.restTemplate;
	}

	public void setRestTemplate(RestTemplate tmpl) {
		this.restTemplate = tmpl;
	}

	public String getRestUrl() {
		return this.restUrl;
	}

	public void setRestUrl(String url) {

	}

	public void setIP(String url) {
		System.out.println("Da bi thay doi  "+url);
		this.restUrl = "http://" + url + ":7080/rest";
	}

	public Response POST(Request request) throws Exception {
		Response response = (Response) restTemplate.postForObject(restUrl + "/post", request, Response.class);
		return response;
	}

	public String POST(String requestJson) throws Exception {
		Request request = JSONSerializer.INSTANCE.fromString(requestJson, Request.class);
		Response response = (Response) restTemplate.postForObject(restUrl + "/post", request, Response.class);
		return JSONSerializer.INSTANCE.toString(response);
	}

	public Response GET(Request request) throws Exception {
		String json = JSONSerializer.INSTANCE.toString(request);
		String url = restUrl + "/get?req=" + URLEncoder.encode(json, "UTF-8");
		Response response = (Response) restTemplate.getForObject(url, Response.class);
		return response;
	}

	public String GET(String requestJson) throws Exception {
		String url = restUrl + "/get?req=" + URLEncoder.encode(requestJson, "UTF-8");
		Response response = (Response) restTemplate.getForObject(url, Response.class);
		return JSONSerializer.INSTANCE.toString(response);
	}
}