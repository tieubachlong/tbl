package com.hkt.client.rest.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class HKTFile {
  private static String appName = "HKTSoft4.0";
  public static String database = "Database";

  private static String defaultDirectory() {
      String OS = System.getProperty("os.name").toUpperCase();
      if (OS.contains("WIN")) {
          return "C:";
      } else if (OS.contains("MAC")) {
          return System.getProperty("user.home") + "/Library/Application "
                  + "Support";
      } else if (OS.contains("NUX")) {
          return System.getProperty("user.home");
      }
      return System.getProperty("user.dir");
  }

  public static File getFileFromFolder(String module, String nameFile) {
      String directory = defaultDirectory() + File.separator
              + appName + File.separator + module;
      if (!new File(directory).exists()) {
          new File(directory).mkdirs();
      }
      String path = directory + File.separator + nameFile;
      return new File(path);
  }

  public static String getDirectoryFolder(String module) {
      String directory = defaultDirectory() + File.separator
              + appName + File.separator + module;
      if (!new File(directory).exists()) {
          new File(directory).mkdirs();
      }
      return directory;
  }
  
  public static String readData(String data) {
    try {
        FileInputStream fi = new FileInputStream(getFileFromFolder("Database", data).getAbsoluteFile());
        ObjectInputStream of = new ObjectInputStream(fi);
        String str = of.readObject().toString();
        of.close();
        return str;
    } catch (Exception e) {
        return "";
        
    }

}
}
