package com.hkt.client.rest;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.util.StringUtils;

import com.hkt.module.core.rest.Request;
import com.hkt.module.core.rest.Response;
import com.hkt.util.json.JSONSerializer;

public class RESTClientIntegrationTest {
  ClientContext clientContext = ClientContext.getInstance() ;
  @Test
  public void testGET() throws Exception {
    RESTClient restClient = clientContext.getBean(RESTClient.class);

    Request request = new Request("core", "PingService", "ping");
    request.setRequestAtTime(System.currentTimeMillis());
    request.setLogEnable(true);

    Response response = restClient.GET(request);
    System.out.println(JSONSerializer.INSTANCE.toString(response));
    assertEquals("Hello", response.getDataAs(String.class));
  }
  
  @Test
  public void test() throws Exception {
    List<String> list = new ArrayList<String>();
    list.add("0,0,1,1");
    list.add("0,1,0,1");
    list.add("1,0,1,2");
    list.add("2,0,2,3");
    list.add("1,0,2,1");
    String ver = "0,0,0,0";
    for(String version: list){
      if(checkVersion(ver, version)){
        ver = version;
        System.out.println(ver);
      }
    }
    System.out.println(ver);
  }
  public boolean checkVersion(String verOld, String verNew) {
    boolean isVersion = true;
   
    String valuesOld[] = verOld.split(",");
    String valuesNew[] = verNew.split(",");
    System.out.println(valuesOld[1]);
    System.out.println(valuesNew[1]);
    if ((valuesOld.length < 4 || valuesOld.length > 5) && (valuesNew.length < 4 || valuesNew.length > 5)) {
      isVersion = false;
    } else {
      try {
        if (Integer.parseInt(valuesNew[0]) > Integer.parseInt(valuesOld[0])) {
          isVersion = true;
          return isVersion;
        } else {
          if (Integer.parseInt(valuesNew[1]) > Integer.parseInt(valuesOld[1])
              && Integer.parseInt(valuesNew[0]) == Integer.parseInt(valuesOld[0])) {
            isVersion = true;
            return isVersion;
          } else {
            if (Integer.parseInt(valuesNew[2]) > Integer.parseInt(valuesOld[2])
                && Integer.parseInt(valuesNew[1]) == Integer.parseInt(valuesOld[1])
                && Integer.parseInt(valuesNew[0]) == Integer.parseInt(valuesOld[0])) {
              isVersion = true;
              return isVersion;
            } else {
              if (Integer.parseInt(valuesNew[3]) > Integer.parseInt(valuesOld[3])
                  && Integer.parseInt(valuesNew[2]) == Integer.parseInt(valuesOld[2])
                  && Integer.parseInt(valuesNew[1]) == Integer.parseInt(valuesOld[1])
                  && Integer.parseInt(valuesNew[0]) == Integer.parseInt(valuesOld[0])) {
                isVersion = true;
                return isVersion;
              } else {
                isVersion = false;
                return isVersion;
              }
            }
          }
        }
      } catch (Exception e) {
        isVersion = false;
      }
    }
    return isVersion;
  }

  @Test
  public void testPOST() throws Exception {
    RESTClient restClient = clientContext.getBean(RESTClient.class);

    Request request = new Request("core", "PingService", "ping");
    request.setRequestAtTime(System.currentTimeMillis());
    request.setLogEnable(true);

    Response response = restClient.POST(request);
    System.out.println(JSONSerializer.INSTANCE.toString(response));
    assertEquals("Hello", response.getDataAs(String.class));
  }
}
