/**
 * 
 */

define([ 'jquery', 'underscore', 'backbone', 'service/service', 'ui/UITable',
		'test/UIPopupFullScreen', 'ui/UICollapsible', 'test/UIBeanScreenTest', 'ui/UIBreadcumbs',
		'module/Module', 'module/account/PriorityUi', 'dialog/Dialog',
		'module/account/Group', 'util/SQL', 'util/DateTime'], function($, _,
		Backbone, service, UITable, UIPopup, UICollapsible, UIBean,
		UIBreadcumbs, module, PriorityUi, UIDialog, Group, sql, DateTime) {
	var UIGroupList = UIBean.extend({
		config: {
			tabs: [
					{
						image: true,
						label: 'Thông tin doanh nghiệp',
						name: 'ThongTinDoanhNghiep',
						fields: [
						        {field: 'loginId', label: 'Mã DN',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'name', label: 'Tên DN',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'phone', label: 'ĐTCĐ',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'fax', label: 'Fax',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'email', label: 'Email',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'group', label: 'Nhóm',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'model', label: 'Mô hình',numberOfTab: 0, toggled : true, filterable : true,
									select: {
										getOptions: function(field){
											var result = [{value: '', label:''},
											              {value: 'nhaHang', label:'Nhà hàng'},
											              {value: 'caPhe', label:'Cà phê'},
											              {value: 'kara', label:'Karaoke'},
											              {value: 'congTyCoPhan', label:'Công ty cổ phần'},
											              ];
											return result;
										}
									}
								},
								{field: 'address', label: 'Địa chỉ',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'slogan', label: 'Slogan',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'manager', label: 'Quản lý',numberOfTab: 0, toggled : true, filterable : true,
									select: {
										getOptions: function(field){
											var result = [
											              {value: 'co', label:'Có'},
											              {value: 'co', label:'Không'},];
											return result;
										}
									}
								},
								{field: 'createdTime', label: 'Bắt đầu',numberOfTab: 0, datepicker: {},},
								{field: 'registrationCode', label: 'Mã số thuế',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'terminatedDate', label: 'Kết thúc',numberOfTab: 0, datepicker: {},},
								{field: 'representative', label: 'Đại diện DN',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'fullName', label: 'Tên đầy đủ',numberOfTab: 0, toggled : true, filterable : true,},
								{field: 'description', label: 'Miêu tả',numberOfTab: 0, textarea : {},},
								{field: 'note', label: 'Ghi chú', numberOfTab: 0, textarea : {},},
						],
						actions: [
						          
						],
					},
					{
						label: 'Đăng ký kinh doanh',
						name: 'DangKyKinhDoanh',
						business: {},
						fields: [
						     	{field: 'fullNameVN', label: 'Tên TV', numberOfTab: 1, toggled : true, filterable : true,},
			                   	{field: 'registrationCodeD', label: 'Mã đăng ký', numberOfTab: 1, toggled : true, filterable : true,},
			                	{field: 'fullNameEN', label: 'Tên TA', numberOfTab: 1, toggled : true, filterable : true,},
			                   	{field: 'taxRegistrationCode', label: 'Mã số thuế', numberOfTab: 1, toggled : true, filterable : true,},
			                   	{field: 'representativeD', label: 'Người ĐD', numberOfTab: 1, toggled : true, filterable : true,},
			                   	{field: 'charterCapital', label: 'Vốn điều lệ', numberOfTab: 1, toggled : true, filterable : true,},
			                   	{field: 'domain', label: 'Lĩnh vực KD', numberOfTab: 1, toggled : true, filterable : true,},
			                   	{field: 'legalCapital', label: 'Vốn PĐ', numberOfTab: 1, toggled : true, filterable : true,},
			                   	{field: 'descriptionDK', label: 'Miêu tả', numberOfTab: 1, textarea : {},},    
						],
						actions: [
						          {
									  label: 'Xóa',
									  image: 'reset3.png',
									  field: 'onDeleteBusiness',
									  action: function(thisUI){
										  var numberOfArray= $('#businesstId').attr('value');
											if(numberOfArray != ''){
												thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations.splice(numberOfArray,1);
												$('#business_'+numberOfArray).remove();
											}
									  }
						          },
						          {
									  label: 'Lưu',
									  image: 'reset3.png',
									  field: 'onSaveBusiness',
									  action: function(thisUI, fieldName){
										  var numberOfArray= $('#businesstId').attr('value');
											if(numberOfArray != ''){
												var length= parseInt(numberOfArray) + 1;
												thisUI.onUpdateBusiness( thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray], fieldName);
												$('#business_'+numberOfArray).replaceWith(
														'<tr class="">' + 
														'<td> '+length+'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray]['fullName.vn'] +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray]['fullName.en'] +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray].registrationCode +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray].taxRegistrationCode +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray].representative +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray].charterCapital +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray].legalCapital +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations[numberOfArray].domain +'</td>'+
														'</tr>'
												);
											}
									  },
						          },
						          {
						        	  label: 'Thêm mới',
						        	  field: 'onNewBusiness',
						        	  image: 'save3.png',
						        	  action: function(thisUI, fieldName){
						        		  thisUI.businessRegistrations.unshift(thisUI.onAddDangKiKinhDoanh());
						        		  thisUI.onUpdateBusiness( thisUI.businessRegistrations[0], fieldName);
						        		  thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations.push(thisUI.businessRegistrations[0]);
											//hien thi row moi them vao
											$(".tableDangKiKinhDoanh table").append( '<tr class="">' + 
													'<td> '+(thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations.length )+'</td>'+
													'<td>' +thisUI.businessRegistrations[0]['fullName.vn'] +'</td>'+
													'<td>' +thisUI.businessRegistrations[0]['fullName.en'] +'</td>'+
													'<td>' +thisUI.businessRegistrations[0].registrationCode +'</td>'+
													'<td>' +thisUI.businessRegistrations[0].taxRegistrationCode +'</td>'+
													'<td>' +thisUI.businessRegistrations[0].representative +'</td>'+
													'<td>' +thisUI.businessRegistrations[0].charterCapital +'</td>'+
													'<td>' +thisUI.businessRegistrations[0].legalCapital +'</td>'+
													'<td>' +thisUI.businessRegistrations[0].domain +'</td>'+
													'</tr>');
						        	  },
						          },
						          {
						        	  label: 'Viết lại',
						        	  image: 'reset3.png',
						        	  field: 'onReset',
						          },
						         ]
					},
					{
						label: 'Thông tin liên lạc',
						name: 'ThongTinLienLac',
						contacts: {},
						fields: [
						        {field: 'addressNumber', label: 'Địa chỉ', numberOfTab: 2, toggled : true, filterable : true,},
								{field: 'phoneC', label: 'ĐTCD', numberOfTab: 2, toggled : true, filterable : true,},
								{field: 'district', label: 'Xã/Phường', numberOfTab: 2, toggled : true, filterable : true,},
								{field: 'mobileC', label: 'ĐTDĐ', numberOfTab: 2, toggled : true, filterable : true,},
								{field: 'street', label: 'Đường', numberOfTab: 2, toggled : true, filterable : true,},
								{field: 'emailC', label: 'Email', numberOfTab: 2, toggled : true, filterable : true,},
								{field: 'country', label: 'Quốc gia', numberOfTab: 2, toggled : true, filterable : true,
									select: {
										getOptions: function(field){
											var result = [{value: 'VN', label:'Việt Nam'},];
											return result;
										}
									}
								},
								{field: 'faxC', label: 'Fax', numberOfTab: 2, toggled : true, filterable : true,},
								{field: 'city', label: 'Tỉnh/TP', numberOfTab: 2, toggled : true, filterable : true,
									select: {
										getOptions: function(field){
											var result = [
											              {value: 'haNoi', label:"Hà Nội"}, 
											              {value: 'HCM', label:"Hồ Chí Minh"},
											              {value: 'haiPhong', label:"Hải Phòng"},
											              {value: 'daNang', label: 'Đà Nẵng'},
											              {value: 'canTho', label: 'Cần Thơ'},];
											return result;
										}
									}
								},
								{field: 'website', label: 'Website', numberOfTab: 2, toggled : true, filterable : true,},
						],
						actions: [
								 { 
									  label: 'Xóa',
									  image: 'reset3.png',
									  field: 'onDeleteContact',
									  action: function(thisUI){
										  var numberOfArray= $('#contactId').attr('value');
											if(numberOfArray != ''){
												thisUI.beanStates[thisUI.name].bean[0].contacts.splice(numberOfArray,1);
												$('#contact_'+numberOfArray).remove();
											}  
									  },
								 },
								 {
									  label: 'Lưu',
									  image: 'reset3.png',
									  field: 'onSaveContact',
									  action: function(thisUI, fieldName){
										  console.log("da vao onsve con");
										  var numberOfArray= $('#contactId').attr('value');
											if(numberOfArray != ''){
												var length= parseInt(numberOfArray) + 1;
												thisUI.onUpdateContact( thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray], fieldName);
												$('#contact_'+numberOfArray).replaceWith(
														'<tr class="">' + 
														'<td> '+length+'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].addressNumber +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].street +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].district +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].country +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].city +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].phone +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].mobile +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].fax +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].email +'</td>'+
														'<td>' +thisUI.beanStates[thisUI.name].bean[0].contacts[numberOfArray].website +'</td>'+
														'</tr>'
												);
											}
									  },
								  },
						          {
						        	  label: 'Thêm mới',
						        	  field: 'onNewContact',
						        	  image: 'save3.png',
						        	  action: function(thisUI, fieldName){
						        		    var tab= thisUI.onFindTabByFieldAction(fieldName);
						        		    thisUI.contacts.unshift(thisUI.onAddThongTinLienLac());
						        		    thisUI.onUpdateContact( thisUI.contacts[0], fieldName);
											thisUI.beanStates[thisUI.name].bean[0].contacts.push(thisUI.contacts[0]);
											//hien thi row moi them vao
											$(".tableThongTinLienLac table").append( '<tr class="">' + 
													'<td> '+(thisUI.beanStates[thisUI.name].bean[0].contacts.length )+' </td>'+
													'<td>' +thisUI.contacts[0].addressNumber +'</td>'+
													'<td>' +thisUI.contacts[0].street +'</td>'+
													'<td>' +thisUI.contacts[0].district +'</td>'+
													'<td>' +thisUI.contacts[0].country +'</td>'+
													'<td>' +thisUI.contacts[0].city +'</td>'+
													'<td>' +thisUI.contacts[0].phone +'</td>'+
													'<td>' +thisUI.contacts[0].mobile +'</td>'+
													'<td>' +thisUI.contacts[0].fax +'</td>'+
													'<td>' +thisUI.contacts[0].email +'</td>'+
													'<td>' +thisUI.contacts[0].website +'</td>'+
													'</tr>');
						        	   }
						          },
						          {
						        	  label: 'Viết lại',
						        	  image: 'reset3.png',
						        	  field: 'onReset',
						          }
						         ]
					}
				],
				actions: 
				         {
				        	 saveBean : function(thisUI){
								var account= service.AccountService.findAccountByLoginId(thisUI.beanStates[thisUI.name].bean[0].loginId).data;
								if(account[0].profiles.userRelationships ==undefined){
									account[0].profiles.userRelationships= [];
								}
								if(account[0].profiles.businessRegistrations ==undefined){
									account[0].profiles.businessRegistrations= [];
								}
								if(account[0].contacts == undefined){
									account[0].contacts= [];
								}
								account[0].contacts=thisUI.beanStates[thisUI.name].bean[0].contacts;
								account[0].profiles.businessRegistrations= thisUI.beanStates[thisUI.name].bean[0].profiles.businessRegistrations;
								account[0].profiles.userRelationships=thisUI.beanStates[thisUI.name].bean[0].profiles.userRelationships;
								account[0].profiles.basic=thisUI.beanStates[thisUI.name].bean[0].profiles.basic;
								console.log(account[0]);
								console.log(thisUI.beanStates);
								service.AccountService.saveAccount(account[0]);
							}
				         },
		},
		refresh: function(){
			var today = new Date();
        	var codeString = moment(today).format('YYYYMMDDHHmmss');
        	return codeString;
		},
		init : function(viewStack) {
			this.viewStack = viewStack;
			var result=[];
				result[0]= {loginId: this.refresh()};
			if($('[name="loginId"]').val() != undefined){
				 result[0] = service.AccountService.getAccountByLoginIdOrg('hkt').data;
				 if(result[0] != undefined){
				//hiển thị thông tin basic
					result[0].fullName = result[0].profiles.basic.fullName;
					result[0].name= result[0].profiles.basic.name;
					result[0].email= result[0].profiles.basic.email;
					result[0].manager= result[0].profiles.basic.manager;
					result[0].slogan= result[0].profiles.basic.slogan;
					result[0].description= result[0].profiles.basic.description;
					result[0].note= result[0].profiles.basic.note;
					result[0].address= result[0].profiles.basic.address;
					result[0].registrationCode= result[0].profiles.basic.registrationCode;
					result[0].terminatedDate= result[0].profiles.basic.terminatedDate;
					result[0].representative= result[0].profiles.basic.representative;
					result[0].phone= result[0].profiles.basic.phone;
					result[0].fax= result[0].profiles.basic.fax;
					if(result[0].contacts == undefined)
						result[0].contacts= [];
					if(result[0].profiles == undefined){
						result[0].profiles= {};
						result[0].profiles.businessRegistrations= [];
						result[0].profiles.basic= {phone: '', name : '', fullName: '', mobile: '',fax: '', address: '', email:this.refresh() + '@hkt.com',
								fax: '', render: '', model: '', manager: '',group: '',  slogan: '', note: '',
								createdTime: '', description: '', registrationCode: '', terminatedDate : '', representative: ''}
					}
				 }
				 else {
					 //làm gì thì làm đi
				 }
			}
			console.log(result);
			this.bind('ORGANIZATION', result);
			return this;
		},
		
	});
	var UIGroups = module.UIScreen.extend({
		initialize : function(options) {
			
		},
		activate : function() {
			this.viewStack = new UIBreadcumbs({
				el : "#workspace"
			});
			this.UIGroupList = new UIGroupList().init(this.viewStack);
			UIPopup.activate(this.UIGroupList);
		},
		deactivate : function() {
			
		}
	});
	return UIGroups;
});