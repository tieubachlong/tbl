define([
	'jquery', 'underscore', 'backbone', 'i18n', 'ui/Validator', 'ui/panel/UIPopupFullScreen', 'text!ui/panel/UIBeanFullScreen.jtpl'], 
	function($,_,Backbone,i18n,Validator,UIPopup,UIBeanTmpl) {
	/**
	 * @type ui.UIBean
	 */
	var UIBean = Backbone.View.extend({
		initialize : function(options) {
			// create a clean config for each instance ;
			this.config = $.extend(true, {}, this.config);
			this.disable = true;
			this.onInit(options);
			_.bindAll(this, 'render', 'onEditAction');
		},

		onInit : function(options) {
		},
		onRefresh : function(options) {
		},


		/** @memberOf ui.UIBean */
		bindArray : function(name,array) {

		},

		getBean : function(name) {
		},

		setBeans: function(beans) {
		      this.config.beanTables = beans ;
		},
	
		
		_template : _.template(UIBeanTmpl),

		render : function() {
			var params = {
				config:  this.config
			};
			$(this.el).html(this._template(params));
			$(this.el).trigger("create");
		},

		events : {
			'click a.onEditAction': 'onEditAction',
			'click a.onExitBean': 'onExitBean',

		},
		onEditAction: function(evt){
			
		},
		onExitBean: function(evt){
			UIPopup.closePopup();
		},
		onToggleBean : function(evt) {
			var beanName = $(evt.target).closest("a").attr('bean');
			this._toggleBean(beanName);
			this.beanStates[beanName].select = true;
			this.render();
		},

	});

	return UIBean;
});