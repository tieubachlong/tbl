/**
 * 
 */

define([ 'jquery', 'underscore', 'backbone', 'service/service', 'ui/UITable',
		'ui/UIPopup', 'ui/UICollapsible', 'test/UIBean/UIBean', 'ui/UIBreadcumbs',
		'module/Module', 'module/account/PriorityUi', 'dialog/Dialog',
		'module/account/Group', 'util/SQL', 'util/DateTime', ], function($, _,
		Backbone, service, UITable, UIPopup, UICollapsible, UIBean,
		UIBreadcumbs, module, PriorityUi, UIDialog, Group, sql, DateTime) {
	
	var UIGroupList = UIBean.extend({
			config: {
				label : 'Thành phố',
				icon: '',
				screen : 'Cities',
				tabs: [
					{
						name: 'Nothing 1',
					},
					{
						name: 'Nothing 2',
					},
					{
						name: 'Nothing 3',
					}
				],
				beans : {
					fields : [
							{
								label : 'Mã', field : 'code', toggled : true, filterable : true,
								
							}, 
							{
								label : 'Tên', field : 'name', toggled : true, filterable : true,
							}, 
							{
								label : 'Mô tả', field : 'description',toggled : true, filterable : true,
							}, 
							{
								label : 'Quốc gia', field : 'codeCountry', toggled : true, filterable : true,
							} ],
					table: [
							{
								label : 'Mã', field : 'code',
								
							}, 
							{
								label : 'Tên', field : 'name',
							}, 
							{
								label : 'Mô tả', field : 'description',
							}, 
							{
								label : 'Quốc gia', field : 'codeCountry'
							}     	 
						 ]
			
				},
				actions: [
				         {
				        	 	label: 'Điền mới', action: 'onClear', icon: 'reset3.png',
				        	 	onClick: function(){
				        	 		console.log('button dien moi');
				        	 	}
				         },
				         {
					        	label: 'Lưu', action: 'onSave', icon: 'save3.png',
				        	 	onClick: function(){
				        	 		console.log("Da kich vao nut luu nay");
				        	 	}
					     },
				         {
					        	label: 'Xem lại', action: 'onRefresh', icon: 'restart3.png',
				        	 	onClick: function(){
				        	 		
				        	 	}
					     },
				         {
					        	label: 'Xóa', action: 'onDelete', icon: 'delete3.png',
				        	 	onClick: function(){
				        	 		
				        	 	}
					     },
					     {
					        	label: 'Sửa', action: 'onEdit', icon: 'sudungtratruoc3.png',
				        	 	onClick: function(){
				        	 		
				        	 	}
					     },
				         {
					        	label: 'Thoát', action: 'onExit', icon: 'quit3.png',
				        	 	onClick: function(){
				        	 		UIPopup.closePopup();
				        	 	}
					     },
				         ],
		},
		init : function(viewStack) {
			this.viewStack = viewStack;
			this.isSave = true;
			/*var result = service.KpiService.getAllCity().data;
			this.setBeans(result);*/
			return this;
		}
	});
	var UIGroups = module.UIScreen.extend({
		initialize : function(options) {
			
			this.groupList = new UIGroupList().init(null);
		},
		activate : function() {
			this.viewStack = new UIBreadcumbs({
				el : "#workspace"
			});
			this.UIGroupList = new UIGroupList().init(this.viewStack);
			UIPopup.activate(this.UIGroupList);
		},
		deactivate : function() {
			
		}
	});
	return UIGroups;
});