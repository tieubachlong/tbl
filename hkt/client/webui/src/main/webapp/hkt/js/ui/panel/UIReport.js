define([
	'jquery', 
	'underscore', 
	'backbone', 
	'i18n', 
	'ui/Validator', 
	'ui/panel/UIPopupFullScreen', 
	'text!ui/panel/UIBeanFullScreen.jtpl',
	'text!ui/panel/UIReport.jtpl',
	 'css!ui/panel/UIReport.css'
	],function($,_,Backbone,i18n,Validator,UIPopup,UIBeanTmpl, UIReportTmpl) {
	
	  var Node = function(id, bean, name, index, icon, color, check) {
		    this.bean = bean;
		    this.name = name;
		    this.id   = id;
		    this.index   = index;
		    this.children = [] ; 
		    this.collapsed = true;
		    this.icon = icon;
		    this.color = color;
		    this.check = check;
		    this.child
		    
		    this.addChild = function(bean, name) {
		      var childId = this.id + "_" + this.children.length;
		      var child = new Node(childId, bean, name, this.index+1) ;
		      this.children.push(child) ;
		      return child;
		    };
		    this.addChild = function(bean, name, icon) {
		        var childId = this.id + "_" + this.children.length;
		        var child = new Node(childId, bean, name, this.index+1,icon) ;
		        this.children.push(child) ;
		        return child;
		      };
		      
		    this.addChild = function(bean, name, icon, color, check) {
		        var childId = this.id + "_" + this.children.length;
		        var child = new Node(childId, bean, name, this.index+1, icon, color, check) ;
		        this.children.push(child) ;
		        return child;
		      };

		    this.getChildren = function() { return this.children; };

		    this.isCollapsed = function() { return this.collapsed; };

		    this.setCollapsed = function(collapsed) { this.collapsed = collapsed; };

		    this.toggleCollapsed = function() { 
		    	this.collapsed = !this.collapsed; 
		    	};

		    this.findDescendant = function(id) {
		      if(this.id == id) return this;
		      for(var i = 0; i < this.children.length; i++) {
		        var found = this.children[i].findDescendant(id);
		        if(found != null) return found ;
		      }
		      return null ;
		    };
		  };
	/**
	 * @type ui.UIBean
	 */
	var UIReport = Backbone.View.extend({
		initialize : function(options) {
			// create a clean config for each instance ;
			this.config = $.extend(true, {}, this.config);
			this.tabConfig={};
			this.bean = {};
			this.beanConfig = {};
			this.nodes = [] ;
			this.actions={};
			this.onInit(options);
			_.bindAll(this, 'render', 'onEditAction');
		},

		onInit : function(options) {
		},
		onRefresh : function(options) {
		},
		
	    /**@memberOf ui.UIBean*/
	    bind: function(name, bean, setDefault) {

	    },

		/** @memberOf ui.UIBean */
		bindArray : function(name,array) {

		},
		
		setBeans: function(){
			this._setDefaultFieldValue(this.beanConfig, this.bean);
		},
		getBeans: function(){
			return this.bean;
		},
		setActions: function(actions){
			for(var x in actions){
				this.actions[actions[x].action] = actions[x];
			}
		},
		getActions : function() {
			return this.actions;
		},
		setTabValue: function(valueName){
			this.valueName = valueName;
		},
		setTabConfig: function(tabs, tabsName){
			var k = tabs.length;
			for(var i=0;i<k; i++){
				this.tabConfig[tabs[i].name] = tabs[i];
			}
			
			this._setBeanTabConfig(this.tabConfig[tabsName]);
		},
		setMenuRight: function(menu){
			this.menuRight = menu;
			this._getBeanFieldConfig(menu);

		},
	    _template: _.template(UIBeanTmpl),
	    
	    _rows: _.template(UIReportTmpl),
	    getNodes: function() { return this.nodes ; },

	    
	    addNode: function(bean, name, icon) {
	        var id = "node_" + this.nodes.length;
	        var node = new Node(id, bean, name, 0, icon) ;
	        this.nodes.push(node);
	        return node ;
	      },
		render : function() {
			var params = {
					config:  this.config,
					actions: this.actions
			};
			$(this.el).html(this._template(params));
			$(this.el).trigger("create");
			 this.renderRows() ;
			 this.onLoadCss();
		},
	    renderRows: function() {
	      var params = { 
	        config: this.config, 
	        valueName: this.valueName, 
	        tabConfig: this.tabConfig,
	        menuRight: this.menuRight, 
	        bean: this.bean, 
	        table: this.table,
	        nodes: this.nodes
	      } ;
	      var tableBlock = $(this.el).find(".UIBeansTab");
	      tableBlock.html(this._rows(params));
	      tableBlock.trigger("create");
	    },
		events : {
			'click a.onEditAction': 'onEditAction',
			'click a.onExitAction': 'onExitAction',
			'click a.onTabAction': 'onTabAction',
			'click a.onviewReport': 'onviewReport',
			'blur   .onComboBox': 'onComboBox',
			'blur  .onBlurDatePickerInput': 'onBlurDatePickerInput',
		    'focus .onFocusDatePickerInput' : "onFocusDatePickerInput",
		    'click .onChangeCheckBox' : "onChangeCheckBox",
		    'click a.onCollapseExpand': 'onCollapseExpand',

		},
		onLoadCss : function(){
			if(window.innerWidth < 700 && window.innerWidth >= 320){
				showThongKeDienThoai();
				//nếu quay ngang thì mình phải gán chiều cao cho nó nữa, gán như nào nhỉ , hic
			}
			console.log(window.innerHeight);
		},
		onEditAction: function(evt){
			var tabName = $(evt.target).closest("a").attr('action') ;
			console.log(tabName);
			var action = this.actions[tabName];
			action.onClick(this);
		},
		onExitAction: function(evt){
			UIPopup.closePopup();
		},
		onTabAction: function(evt){
			var value = $(evt.target).closest("a");
			  var tabName = value.attr('name') ;
			  this.valueName = tabName;
			  var tabs = value.find("div."+tabName);
			  tabs.css('display', 'inline');
			  this.render();
		},
	    onCollapseExpand: function(evt) {
	        var nodeId = $(evt.target).attr('nodeId') ;
	        var node = this._findNode(nodeId) ;
	        this.addChild(node);
	        node.toggleCollapsed() ;
	        this.render() ;
	      },
	      _findNode: function(nodeId) {
	          for(var i = 0; i < this.nodes.length; i++) {
	            var found = this.nodes[i].findDescendant(nodeId) ;
	            if(found != null) return found ;
	          }
	          return null ;
	        },
		onComboBox: function(evt){
			var combo = $(evt.target).closest("select").attr('name') ;
			var value     = $(evt.target).val() ;
			this.bean[combo] = value;
			if(this.beanConfig[combo].select !=null){
				if(this.beanConfig[combo].select.setOptions !=null){
					this.beanConfig[combo].select.setOptions(this, this.bean);
				}
			}
			if(this.beanConfig[combo].setOptions !=null){
				this.beanConfig[combo].setOptions(this, this.bean);
			}
			if(this.beanConfig[combo].fieldLoad !=null){
				
				 var fieldValue;
		          if(this.beanConfig[combo].fieldLoad instanceof Array) {
		              fieldValue = this.beanConfig[combo].fieldLoad.slice();
		            } else {
		              fieldValue = this.beanConfig[combo].fieldLoad ;
		            }
		          this.beanConfig[fieldValue].getOptions("",this.bean);
		          
			}
			this.render();
		},
		onviewReport: function(evt){
			var viewInput = $(evt.target).closest("a").attr('name') ;
			this.beanConfig[viewInput].action(this);
		},
		onChangeCheckBox: function(evt){
			var fieldName = $(evt.target).attr('name');
			var value = $(evt.target).closest("div.ui-checkbox");
			var isCheck = value.context.checked;
			bean[fieldName] = isCheck;
		},
	    onFocusDatePickerInput: function(evt) {
	        evt.stopPropagation();
	      },

	      onBlurDatePickerInput: function(evt) {
	    	var dateName = $(evt.target).closest("input").attr('name') ;
	        //console.log('on focus ' + fieldConfig.datepicker.field) ;
	        var dateVal = $(evt.target).val() ;
	        var beanConfig = this.beanConfig;
	        var bean = this.bean;
	        setTimeout(function() {
	          var dateVal = $(evt.target).val() ;
	          beanConfig[dateName].datepicker.setDate(dateVal, bean) ;
	        }, 300) ;
	      },
		
		disableField: function(fieldName){
			this.beanConfig[fieldName].disable = false;
			this.render();
		},
		_getBeanFieldConfig(config){
			for(var x in config){
				var k = config[x].fields;
				for(var y in k){
					if(k[y].multiSelect){
						var selects = k[y].select;
						for(var z in selects){
							this.beanConfig[selects[z].field] = selects[z];
						}
					}else{
						this.beanConfig[k[y].field] = k[y];
					}
				}
			}
		},
	    _getTabConfig: function(name) {
	        for(var i = 0; i < this.config.tabs.length; i++) {
	          var tab = this.config.tabs[i] ;
	          if(name == tab.name) return tab ;
	        }
	        return null ;
	      },
	      _setBeanTabConfig: function(beanConfig){
	    	  k = beanConfig.rows.length;
	    	for(var i in beanConfig.rows){
	    		var fields = beanConfig.rows[i].fields;
	    		for(var j in fields){
	    			this.beanConfig[fields[j].field] = fields[j];
	    		}
	    		
	    	}  
	      },
	      _setDefaultFieldValue: function(beanConfig, bean) {
	    	  for(var x in beanConfig){
	    		  var fieldValue = bean[beanConfig[x].field] ;
	    		  if(fieldValue ==null || fieldValue == undefined){
	    			  if(beanConfig[x].datepicker !=null){
	    				  bean[beanConfig[x].field] =  beanConfig[x].datepicker.getDate(bean);
	    			  }else{
	    				  if(beanConfig[x].checkBox !=null){
	    					  bean[beanConfig[x].field] =  beanConfig[x].checkBox.value;
	    				  }else{
		    				  if(beanConfig[x].select !=null){
			    				  var options = beanConfig[x].select.getOptions("", bean) ; 
			    				  bean[beanConfig[x].field] =  options[0].value; 
			    			  }else {
			    				  if(beanConfig[x].getOptions != null){
			    					 var  options = beanConfig[x].getOptions("", bean) ;
			    					 bean[beanConfig[x].field] =  options[0].value; 
			    			  }
			    			  }
	    				  }
	    			  }
	    		  }
	    	  }
	      }

	});

	return UIReport;
});