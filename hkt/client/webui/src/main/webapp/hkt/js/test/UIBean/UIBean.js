define([ 'jquery', 'underscore', 'backbone', 'i18n', 'ui/Validator',
		'ui/UIPopup', 'text!test/UIBean/UIBean.jtpl', 'css!test/UIBean.css',
		'service/service' ], function($, _, Backbone, i18n, Validator, UIPopup,
		UIBeanTmpl, service) {
	/**
	 * @type ui.UIBean
	 */
	var UIBean = Backbone.View.extend({

		initialize : function(options) {
			// create a clean config for each instance ;
			this.config = $.extend(true, {}, this.config);
			this.disable = true;
			this.onInit(options);
			_.bindAll(this, 'render', 'onClosePopupBean', 'onClearBean',
					'onSaveBean', 'onRefreshBean', 'onDeleteBean',
					'onEditBean', 'onViewMode', 'onEditAction');
		},

		onInit : function(options) {
		},
		onRefresh : function(options) {
		},

		/** @memberOf ui.UIBean */
		bind : function(name, bean, setDefault) {

		},

		/** @memberOf ui.UIBean */
		bindArray : function(name, array) {

		},

		getBean : function(name) {
		},

		setBeans : function(beans) {
			this.config.beanTables = beans;
		},
		setLabel : function(label){
			this.label= label;
		},
		_template : _.template(UIBeanTmpl),

		render : function() {
			var params = {
				title : this.label,
				beans : this.beans,
				config : this.config,
				disable : this.disable
			};
			$(this.el).html(this._template(params));
			$(this.el).trigger("create");
			console.log(this);
		},

		events : {
			'click a.thoatBean' : 'onClosePopupBean',
			'click a.dienmoiBean' : 'onClearBean',
			'click a.luuBean' : 'onSaveBean',
			'click a.xemlaiBean' : 'onRefreshBean',
			'click a.xoaBean' : 'onDeleteBean',
			'click a.suaBean' : 'onEditBean',
			'click a.onEditAction' : 'onEditAction'
		},
		onEditBean : function() {
			// edit bean
		},
		onClosePopupBean : function() {
			// thoat bean
			UIPopup.closePopup();
		},
		onClearBean : function() {
			// dien moi bean
		},
		onSaveBean : function(city) {
			// luu bean
			console.log("Nut luu ben uiBean");
		},
		onRefreshBean : function() {
			// xem lai beans
		},
		onDeleteBean : function() {
			// xoa bean
		},
		onToggleBean : function(evt) {
			var beanName = $(evt.target).closest("a").attr('bean');
			this._toggleBean(beanName);
			this.beanStates[beanName].select = true;
			this.render();
		},
		onViewMode : function(evt) {
			var beanName = $(evt.target).closest("[bean]").attr('bean');
			// console.log(beanName);
		
			var beanState = this.beanStates[beanName];
			var bean = beanState.bean;
			var beanConfig = this._getBeanConfig(beanName);
			var fields = beanConfig.fields;
			for (var i = 0; i < fields.length; i++) {
				var fieldName = fields[i].field;
				var value = bean[fieldName];
				if (fieldName == "code") {
					var today = new Date();
					value = moment(today).format('YYYYMMDDHHmmss');
				} else {
					value = "";
				}
			}
		},
		onEditAction : function(evt) {
			var beanName = $(evt.target).closest("[bean]").attr('bean');
			var beanConfig = this._getBeanConfig(beanName);
			var beanState = this.beanStates[beanName];
			var actionName = $(evt.target).closest("a").attr("action");
			var actions = beanConfig.edit.actions;
			for (var i = 0; i < actions.length; i++) {
				if (actions[i].action == actionName) {
					actions[i].onClick(this, beanConfig, beanState);
					return;
				}
			}
		},

	});

	return UIBean;
});