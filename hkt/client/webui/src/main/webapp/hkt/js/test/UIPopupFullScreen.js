/**
 * 
 */define([
  'jquery', 
  'underscore', 
  'backbone',
  'dialog/DialogNotice',
  'view/NavigationView',
  'text!module/account/ListMenu.jtpl',
  'i18n',
], function($, _, Backbone,UIDialog, NavigationView, ListMenu, i18n) {
  var UIPopup = Backbone.View.extend({
    el: "#PopupPanelFullScreen",
    
    initialize: function (options) {
    	
    },
    
    activate: function(view) {
      //$('#quangCao').css({display:"none"});
      $(this.el).off();
      var instance = this ;
      view.closePopup = function() {
        instance.closePopup() ;
      };
      this.view = view ;
      this.render() ;
    },
    
    showNotice: function(notice) {
    	var dialog = {nameDialog: "", isDialog: true, kpi: []};
   		dialog.nameDialog = notice;
   		dialog.isDialog = true; 
   	   this.activate(new UIDialog.UIDialogNotice().init(this,dialog));
	},
	
	 getPermission: function(screen, permis){
	    	var per = "";
	    	if(this.accountConfig.loginId=="admin"){
	    		return true;
	    	}else{
	    		if(this.accountConfig.profiles.config!=null &&
	    				this.accountConfig.profiles.config[screen]!=null){
	    			per= this.accountConfig.profiles.config[screen];
	    			if(per=="not"){
	    				this.showNotice("Bạn chưa có quyền này");
	        			return false;
	    			}else{
	    				if(per=="read" && per!=permis){
	    					this.showNotice("Bạn chưa có quyền này");
	    		    		return false;
	    		    	}
	    		    	if((per == "write") && (permis == "all")){
	    		    		this.showNotice("Bạn chưa có quyền này");
	    		    		return false;
	    		    	}
	    			}
	    		}else{
	    			this.showNotice("Bạn chưa có quyền này");
	    			return false;
	    		}
	    		
	    	}
	    	
	    	return true;
	    },
    _template: _.template(
      '<div class="nothing1Popup">' +
      		'<h4 style="color:#A52A2A; text-align:right;">HKT Sofware</h4>'+
      '<div class="nothing2Popup">'+
      		'<img class="imageTitleNothing2" alt="" src="<%=image%>">'+
      		'<h4 style="color:#A52A2A; margin-bottom: 10px"><%=title%></h4>' +
      '<div class="nothing3Popup">' +
      		'<div class="UIPopupContent"></div>' +
      '</div>'+
      '</div>'+
      '</div>'
    ),
    
    render: function() {
      var params = {
        title: this.view.label, image: this.view.image  
      } ;
      $(this.el).html(this._template(params));
      $(this.el).trigger("create") ;
      
      this.view.setElement(this.$('.UIPopupContent')).render();
      $(this.el).popup( "open", {});
    },
    
    
    closePopup: function() {
      $(this.el).popup("close") ;
      this.onMenuClick(menuIdClick, moduleOfNavigation);
    },
    //hien giao dien sau khi thoat popup
    onMenuClick : function(menuId,moduleManager ) {
		var listMenu = [];
		var listUnderMenuItems = [];
		var listUnderImages = [];
		var listImages = [];
		var moduleName = null;
		var moduleUnderName = null;
		moduleManager.getModules().forEach(function(module) {
			if (module.config.name == menuId) {
				moduleName = module.config.name;
				module.screens.forEach(function(screen) {
					if (screen.config.underName == "1") {
						listImages.push(screen.config.icon);
						listMenu.push(screen.config.name);
						
					} else if (screen.config.underName == "2")
					{
						listUnderMenuItems.push(screen.config.name);
						listUnderImages.push(screen.config.icon);
					}
					
					if (screen.config.moduleName!=null){
						moduleUnderName = screen.config.moduleName;
					}
					
					
				});
			}
		});
		var ListMenuView = Backbone.View.extend({
			el : $("#workspace"),
			model : null,
			module : null,
			moduleName : null,
			moduleUnderName : null,
			listImages : [],
			listUnderImages : [],
			listUnderMenuItems : [],
			listMenuItem : [],
			_template : _.template(ListMenu),
			initialize : function() {
				_.bindAll(this, 'render', 'onClickMenuItem', 'onMenuCol');
				this.listMenuItem = listMenu;
				this.module = menuId;
				this.moduleName = moduleName;
				this.listImages = listImages;
				this.listUnderImages = listUnderImages;
				this.listUnderMenuItems = listUnderMenuItems;
				this.moduleUnderName = moduleUnderName;
				this.render();
			},
			render : function() {
				var res = i18n.getResource('view/navigation');
				var params = {
					res : res,
					listMenuItem : this.listMenuItem,
					module : this.module,
					moduleName : this.moduleName,
					listImages : this.listImages,
					listUnderMenuItems : this.listUnderMenuItems,
					listUnderImages : this.listUnderImages,
					moduleUnderName : this.moduleUnderName
				};
				$(this.el).html(this._template(params));
				$(this.el).trigger("create");
			},
			events : {
				'click a.onClickMenuItem' : 'onClickMenuItem',
				'click div.onClickMenuItem' : 'onClickMenuItem',
				'click div.menuCol':'onMenuCol'
			},
			onMenuCol: function(evt)
			{
				var moduleId = $(evt.target).attr("moduleId");
				console.log(moduleId);
				app.view.WorkspaceView.activateModule(moduleId);
			},
			onClickMenuItem : function(evt) {
				var moduleId = $(evt.target).attr("moduleId");
				app.view.WorkspaceView.activateModule(moduleId);
			},
			activate : function() {
				this.render();
			},

			deactivate : function() {
			}
		});
	new ListMenuView();
	},
  });
  
  return new UIPopup() ;
});
