define([
	'jquery', 'underscore', 'backbone', 'i18n', 'ui/Validator', 'ui/UIPopup', 'text!test/UIBean.jtpl', 'css!test/UIBean.css'
], function($,_,Backbone,i18n,Validator,UIPopup,UIBeanTmpl) {
	/**
	 * @type ui.UIBean
	 */
	var UIBean = Backbone.View.extend({

		initialize : function(options) {
			// create a clean config for each instance ;
			this.config = $.extend(true, {}, this.config);
			this.disable = true;
			//console.log(this.config);
			this.onInit(options);
			_.bindAll(this, 'render', 'onClosePopupBean', 'onClearBean','onSaveBean','onRefreshBean','onDeleteBean','onEditBean');
		},

		onInit : function(options) {
		},
		onRefresh : function(options) {
		},

		/** @memberOf ui.UIBean */
		bind : function(name,bean,setDefault) {

		},

		/** @memberOf ui.UIBean */
		bindArray : function(name,array) {

		},

		getBean : function(name) {
		},

		setBeans: function(beans) {
		      this.config.beanTables = beans ;
		},
	
		
		_template : _.template(UIBeanTmpl),

		render : function() {
			var params = {
					title: this.label,
					beans : this.beans, config:  this.config, disable:this.disable
			};
			$(this.el).html(this._template(params));
			$(this.el).trigger("create");
		},

		events : {
			'click a.thoatBean': 'onClosePopupBean',
			'click a.dienmoiBean': 'onClearBean',
			'click a.luuBean': 'onSaveBean',
			'click a.xemlaiBean': 'onRefreshBean',
			'click a.xoaBean': 'onDeleteBean',
			'click a.suaBean': 'onEditBean',
		},
		onEditBean : function (){
			//edit bean
		},
		onClosePopupBean : function (){
			//thoat bean
			UIPopup.closePopup();
		},
		onClearBean : function (){
			//dien moi bean
		},
		onSaveBean : function (){
			// luu bean
		},
		onRefreshBean : function (){
			//xem lai beans
		},
		onDeleteBean : function (){
			//xoa bean
		},
		onToggleBean : function(evt) {
			var beanName = $(evt.target).closest("a").attr('bean');
			this._toggleBean(beanName);
			this.beanStates[beanName].select = true;
			this.render();
		},

	});

	return UIBean;
});