define([
  'jquery', 
  'underscore', 
  'backbone',
  'dialog/DialogNotice'
], function($, _, Backbone,UIDialog) {
  var UIPopup = Backbone.View.extend({
    el: "#PopupPanel",
    
    initialize: function (options) {
    },
    
    activate: function(view) {
      $(this.el).off();
      var instance = this ;
      view.clopsePopup = function() {
        instance.clopsePopup() ;
      };
      this.view = view ;
      this.render() ;
    },
    
    showNotice: function(notice) {
    	var dialog = {nameDialog: "", isDialog: true, kpi: []};
   		dialog.nameDialog = notice;
   		dialog.isDialog = true; 
   	   this.activate(new UIDialog.UIDialogNotice().init(this,dialog));
	},
	
	 getPermission: function(screen, permis){
	    	var per = "";
//	    	if(this.accountConfig.loginId=="admin"){
//	    		return true;
//	    	}else{
//	    		if(this.accountConfig.profiles.config!=null &&
//	    				this.accountConfig.profiles.config[screen]!=null){
//	    			per= this.accountConfig.profiles.config[screen];
//	    			if(per=="not"){
//	    				this.showNotice("Bạn chưa có quyền này");
//	        			return false;
//	    			}else{
//	    				if(per=="read" && per!=permis){
//	    					this.showNotice("Bạn chưa có quyền này");
//	    		    		return false;
//	    		    	}
//	    		    	if((per == "write") && (permis == "all")){
//	    		    		this.showNotice("Bạn chưa có quyền này");
//	    		    		return false;
//	    		    	}
//	    			}
//	    		}else{
//	    			this.showNotice("Bạn chưa có quyền này");
//	    			return false;
//	    		}
//	    		
//	    	}
	    	
	    	return true;
	    },
	    
	
	
    _template: _.template(
    		'<div class="nothing1">' +
      		'<h4 style="color:#A52A2A; text-align:right;">HKT Sofware</h4>'+
      '<div class="nothing2">'+
      		'<img class="imageTitleNothing2"alt="" src="images/banggia3.png">'+
      		'<h4 style="color:#A52A2A; margin-bottom: 10px"><%=title%></h4>' +
      '<div class="nothing3">' +
      		'<div class="UIPopupContent"></div>' +
      '</div>'+
      '</div>'+
      '</div>'
    ),
    
    render: function() {
      var params = {
        title: this.view.label  
      } ;
      $(this.el).html(this._template(params));
      $(this.el).trigger("create") ;
      
      this.view.setElement(this.$('.UIPopupContent')).render();
      $(this.el).popup( "open", {});
    },
    
    closePopupChoice: function (){
    	$(this.el).popup("close") ;
    },
    closePopup: function() {
      $(this.el).popup("close") ;
      this.onMenuClick(menuIdClick, moduleOfNavigation);
    },
    //hien thi giao dien sau khi dong popup
    onMenuClick : function(menuId,moduleManager ) {
		var listMenu = [];
		var listUnderMenuItems = [];
		var listUnderImages = [];
		var listImages = [];
		var moduleName = null;
		var moduleUnderName = null;
		moduleManager.getModules().forEach(function(module) {
			if (module.config.name == menuId) {
				moduleName = module.config.name;
				module.screens.forEach(function(screen) {
					if (screen.config.underName == "1") {
						listImages.push(screen.config.icon);
						listMenu.push(screen.config.name);
						
					} else if (screen.config.underName == "2")
					{
						listUnderMenuItems.push(screen.config.name);
						listUnderImages.push(screen.config.icon);
					}
					
					if (screen.config.moduleName!=null){
						moduleUnderName = screen.config.moduleName;
					}
					
					
				});
			}
		});
		var ListMenuView = Backbone.View.extend({
			el : $("#workspace"),
			model : null,
			module : null,
			moduleName : null,
			moduleUnderName : null,
			listImages : [],
			listUnderImages : [],
			listUnderMenuItems : [],
			listMenuItem : [],
			_template : _.template(ListMenu),
			initialize : function() {
				_.bindAll(this, 'render', 'onClickMenuItem', 'onMenuCol');
				this.listMenuItem = listMenu;
				this.module = menuId;
				this.moduleName = moduleName;
				this.listImages = listImages;
				this.listUnderImages = listUnderImages;
				this.listUnderMenuItems = listUnderMenuItems;
				this.moduleUnderName = moduleUnderName;
				this.render();
			},
			render : function() {
				var res = i18n.getResource('view/navigation');
				var params = {
					res : res,
					listMenuItem : this.listMenuItem,
					module : this.module,
					moduleName : this.moduleName,
					listImages : this.listImages,
					listUnderMenuItems : this.listUnderMenuItems,
					listUnderImages : this.listUnderImages,
					moduleUnderName : this.moduleUnderName
				};
				$(this.el).html(this._template(params));
				$(this.el).trigger("create");
			},
			events : {
				'click a.onClickMenuItem' : 'onClickMenuItem',
				'click div.onClickMenuItem' : 'onClickMenuItem',
				'click div.menuCol':'onMenuCol'
			},
			onMenuCol: function(evt)
			{
				var moduleId = $(evt.target).attr("moduleId");
				console.log(moduleId);
				app.view.WorkspaceView.activateModule(moduleId);
			},
			onClickMenuItem : function(evt) {
				var moduleId = $(evt.target).attr("moduleId");
				app.view.WorkspaceView.activateModule(moduleId);
			},
			activate : function() {
				this.render();
			},

			deactivate : function() {
			}
		});
	new ListMenuView();
	},
  });
  
  return new UIPopup() ;
});
