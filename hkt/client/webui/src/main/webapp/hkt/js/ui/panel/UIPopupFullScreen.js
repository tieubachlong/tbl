/**
 * 
 */define([
  'jquery', 
  'underscore', 
  'backbone',
  'dialog/DialogNotice'
], function($, _, Backbone,UIDialog) {
  var UIPopup = Backbone.View.extend({
    el: "#PopupPanelFullScreen",
    
    initialize: function (options) {
    	
    },
    
    activate: function(view) {
      $(this.el).off();
      var instance = this ;
      view.closePopup = function() {
        instance.closePopup() ;
      };
      this.view = view ;
      this.render() ;
    },
    
    showNotice: function(notice) {
    	var dialog = {nameDialog: "", isDialog: true, kpi: []};
   		dialog.nameDialog = notice;
   		dialog.isDialog = true; 
   	   this.activate(new UIDialog.UIDialogNotice().init(this,dialog));
	},
	
	 getPermission: function(screen, permis){
	    	var per = "";
	    	if(this.accountConfig.loginId=="admin"){
	    		return true;
	    	}else{
	    		if(this.accountConfig.profiles.config!=null &&
	    				this.accountConfig.profiles.config[screen]!=null){
	    			per= this.accountConfig.profiles.config[screen];
	    			if(per=="not"){
	    				this.showNotice("Bạn chưa có quyền này");
	        			return false;
	    			}else{
	    				if(per=="read" && per!=permis){
	    					this.showNotice("Bạn chưa có quyền này");
	    		    		return false;
	    		    	}
	    		    	if((per == "write") && (permis == "all")){
	    		    		this.showNotice("Bạn chưa có quyền này");
	    		    		return false;
	    		    	}
	    			}
	    		}else{
	    			this.showNotice("Bạn chưa có quyền này");
	    			return false;
	    		}
	    		
	    	}
	    	
	    	return true;
	    },
    _template: _.template(
      '<div class="nothing1Popup">' +
      		'<h4 style="color:#A52A2A; text-align:right;">HKT Sofware</h4>'+
      '<div class="nothing2Popup">'+
      		'<img class="imageTitleNothing2" alt="" src="<%=image%>">'+
      		'<h4 style="color:#A52A2A; margin-bottom: 10px"><%=title%></h4>' +
      '<div class="nothing3Popup">' +
      		'<div class="UIPopupContent"></div>' +
      '</div>'+
      '</div>'+
      '</div>'
    ),
    
    render: function() {
      var params = {
        title: this.view.label, image: this.view.image  
      } ;
      $(this.el).html(this._template(params));
      $(this.el).trigger("create") ;
      
      this.view.setElement(this.$('.UIPopupContent')).render();
      $(this.el).popup( "open", {});
    },
    
    closePopup: function() {
      $(this.el).popup("close") ;
    },
  });
  
  return new UIPopup() ;
});
