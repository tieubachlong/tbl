define([ 'jquery', 'underscore', 'backbone', 'i18n', 'ui/Validator',
		'test/UIPopupFullScreen', 'text!test/FullScreen.jtpl',
		'view/NavigationView' ], function($, _, Backbone, i18n, Validator,
		UIPopup, UIBeanTmpl, NavigationView) {
	/**
	 * @type ui.UIBean
	 */
	var UIBean = Backbone.View
			.extend({
				initialize : function(options) {
					// create a clean config for each instance ;
					this.config = $.extend(true, {}, this.config);
					this.config1 = $.extend(true, {}, this.config1);
					this.beanStates = {};
					this.businessRegistrations=[];
					this.userRelationships= [];
					this.contacts=[];
					this.disable = true;
					this.onInit(options);
					
					_.bindAll(this, 'render', 'onClosePopupBean',
							'onClearBean', 'onSaveBean', 'onRefreshBean',
							'onDeleteBean', 'onEditBean', 'onLoadTab');
				},

				onInit : function(options) {
				},
				onRefresh : function(options) {
				},
				bind : function(name, bean, setDefault) {
					if (setDefault) {
						var beanConfig = this.config1;
						this._setDefaultFieldValue(beanConfig, bean);
					}
					var size = Object.keys(this.beanStates).length;
					var select = false;
					if (size == 0)
						select = true;
					this.beanStates[name] = {
						bean : bean,
						editMode : false,
					};
					this.name= name;
				},
				onAddDangKiKinhDoanh: function(){
					var iconKinhDoanh ={'fullName.vn': '', 'registrationCode': '', 'fullName.en': '', 'taxRegistrationCode': '', 
							'representative': '','charterCapital': '', 'domain': '', 'legalCapital': '',
							'description': ''};
					return iconKinhDoanh;
				},
				onAddThongTinLienLac: function(){
					var iconLienLac  ={'addressNumber': '', 'phone': [], 'district' : '', 'mobile' : [], 
							'street': '','email': [], 'country': '', 'fax': [], 'website': [], 'city': ''}
					return iconLienLac;
				},

				/** @memberOf ui.UIBean */
				bindArray : function(name, array) {

				},
				getBeanState : function(name) {
					return this.beanStates[name];
				},
				getBean : function(name) {
					return this.beanStates[name].bean;
				},

				setBeans : function(beans) {
					this.config.beanTables = beans;
				},

				_template : _.template(UIBeanTmpl),

				render : function() {
					var params = {
						title : this.label,
						beans : this.beans,
						config : this.config,
						config1 : this.config1,
						disable : this.disable,
						beanStates : this.beanStates,
					};
					$(this.el).html(this._template(params));
					$(this.el).trigger("create");
					$('li.' + this.config.tabs[0].name).addClass('active');
					$('div#' + this.config.tabs[0].name).addClass('active');
					this.onLoadTab();
				},
				events : {
					'click a.onEditAction' : 'onEditAction',
					'click a.onExitBean' : 'onExitBean',
					'click a.thoatBean' : 'onClosePopupBean',
					'click a.dienmoiBean' : 'onClearBean',
					'click a.luuBean' : 'onSaveBean',
					'click a.xemlaiBean' : 'onRefreshBean',
					'click a.xoaBean' : 'onDeleteBean',
					'click a.suaBean' : 'onEditBean',
					'click a.tabMenu' : 'onClickTabMenu',
					'blur .onChangeInput' : 'onChangeInputDoanhNghiep',
					'click .clickDoanhNghiep': 'onClickDoanhNghiep',
					'dblclick .onClichEditTrDoanhNghiep': 'onClickEditTrContact',
					'dblclick .onClickEditBusiness': 'onClickEditBusiness'
				},
				//tim tab by action.field
				onFindTabByFieldAction: function(name){
					for(var i=0; i<this.config.tabs.length; i++){
						if(this.config.tabs[i].actions !=undefined){
							for(var j=0; j<this.config.tabs[i].actions.length; j++){
								if(this.config.tabs[i].actions[j].field==name)
									return i;
							}
						}
					}
					return 0;
				},
				
				onClickDoanhNghiep: function(evt){
					var fieldName= $(evt.target).attr('name');
					//Xóa đăng ký kinh doanh
					if(fieldName == 'onDeleteBusiness'){
						this.config.tabs[1].actions[0].action(this);
					}
					/// Xóa thông tin liên lạc
					if(fieldName == 'onDeleteContact'){
						this.config.tabs[2].actions[0].action(this);
					}
					//thêm đăng ký kinh doanh
					if(fieldName=='onNewBusiness'){
						this.config.tabs[1].actions[2].action(this, fieldName);
					}
					//Thêm thông tin liên lạc
					if(fieldName=='onNewContact'){
						this.config.tabs[2].actions[2].action(this, fieldName);
					}
					//sửa thông tin contact
					if(fieldName == 'onSaveContact'){
						this.config.tabs[2].actions[1].action(this, fieldName);
					}
					//sửa thông tin business
					if(fieldName == 'onSaveBusiness'){
						this.config.tabs[1].actions[1].action(this, fieldName);
					}
				},
				//kiểm tra field đó ở tab nào
				onCheckFieldDoanhNghiep: function(field1){
					for(var i=0; i< this.config1.fields.length; i++){
						if( this.config1.fields[i].field === field1){
							return this.config1.fields[i].numberOfTab;
							break;
						}
					}
					return -1;
				},
				//thay đổi giá trị field khi chỉnh sửa
				onChangeInputDoanhNghiep : function(evt){
					var beanName = this.name;
					var beanState = this.beanStates[beanName];
					var beanConfig = this.config1;
					var fieldName = $(evt.target).attr('name');
					var bean = null;
					if (this.onCheckFieldDoanhNghiep(fieldName) == 0)
						bean = beanState.bean[0].profiles.basic;
					else bean = beanState.bean[0];
						var fieldConfig = this._getBeanFieldConfig(beanConfig,
								fieldName);
						var value = $(evt.target).val();
						var dotIdx = fieldName.lastIndexOf("@");
						var idx = -1;
						if (dotIdx > 0) {
							idx = parseInt(fieldName.substring(dotIdx + 1));
							fieldName = fieldName.substring(0, dotIdx);
						}
						var field = this._getBeanFieldConfig(beanConfig,
								fieldName);
						if (field.multiple) {
							if (bean[fieldName][idx] != value
									&& beanState.origin == null) {
								beanState.origin = $.extend(true, {}, bean);
							}
							bean[fieldName][idx] = value;
						} else {
							if (bean[fieldName] != value
									&& beanState.origin == null) {
								beanState.origin = $.extend(true, {}, bean);
							}
							bean[fieldName] = value;
							console.log(value);
						}
				},
				
				onChangeInput : function(evt) {
					var beanName = this.name;
					var beanState = this.beanStates[beanName];
					var beanConfig = this.config1;
					var bean = beanState.bean[0];
					var fieldName = $(evt.target).attr('name');
						var fieldConfig = this._getBeanFieldConfig(beanConfig,
								fieldName);
						var value = $(evt.target).val();
						var dotIdx = fieldName.lastIndexOf("@");
						var idx = -1;
						if (dotIdx > 0) {
							idx = parseInt(fieldName.substring(dotIdx + 1));
							fieldName = fieldName.substring(0, dotIdx);
						}

						var field = this._getBeanFieldConfig(beanConfig,
								fieldName);
						if (field.multiple) {
							if (bean[fieldName][idx] != value
									&& beanState.origin == null) {
								beanState.origin = $.extend(true, {}, bean);
							}
							bean[fieldName][idx] = value;
						} else {
							if (bean[fieldName] != value
									&& beanState.origin == null) {
								beanState.origin = $.extend(true, {}, bean);
							}
							bean[fieldName] = value;
							//console.log(bean[fieldName]);
						}
				},
				
				_getBeanFieldConfig : function(beanConfig, fieldName) {
					for (var i = 0; i < beanConfig.fields.length; i++) {
						var field = beanConfig.fields[i];
						if (fieldName == field.field)
							return field;
					}
				},
				_setDefaultFieldValue : function(beanConfig, bean) {
					var fields = beanConfig.fields;
					for (var i = 0; i < fields.length; i++) {
						var field = fields[i];
						var fieldValue = bean[field.field];
						if (fieldValue == null && field.defaultValue != null) {
							if (field.defaultValue instanceof Array) {
								fieldValue = field.defaultValue.slice();
							} else {
								fieldValue = field.defaultValue;
							}
							bean[field.field] = fieldValue;
						} else if (fieldValue == null && field.select != null) {
							var options = field.select.getOptions("", bean);
							if (options != null && options.length > 0) {
								bean[field.field] = options[0].value;
							}
						}
					}
				},
				_getBeanConfig : function(beanName) {
					if (this.config.type == 'array') {
						var idx = beanName.lastIndexOf('_');
						var configName = beanName.substring(0, idx);
						return this.config.beans[configName];
					} else {
						console.log(this.config.beans[beanName]);
						return this.config.beans[beanName];
					}
				},
				// load tab trong full popup
				onLoadTab : function() {
					var beanStates = this.beanStates;
					var beanStateName = this.getBeanState(name);
					for (var i = 0; i < this.config.tabs.length; i++) {
						var tabConfig = [];
						var config= null
						for (var j = 0; j < this.config1.fields.length; j++) {
							if (this.config1.fields[j].numberOfTab == i) {
								tabConfig.push(this.config1.fields[j]);
								config=this.config.tabs[i];
							}
						}
						var ListMenuView = Backbone.View.extend({
							el : $('#' + this.config.tabs[i].name),
							_template : _.template(this.config.tabs[i].note),
							initialize : function() {
								this.tabConfig = tabConfig;
								this.beanStates = beanStates;
								this.config= config;
								this.disable = true;
								_.bindAll(this, 'render');
								this.render();
							},
							render : function() {
								var params = {
									tabConfig : this.tabConfig,
									disable : this.disable,
									beanStates : this.beanStates,
									config : this.config,
								};
								$(this.el).html(this._template(params));
							},
						});
						new ListMenuView();
					}
				},
				// chon tab active trong full popup
				onClickTabMenu : function(evt) {
					var tabName = $(evt.target).closest("a").attr('tab');
					for (var i = 0; i < this.config.tabs.length; i++) {
						$('li.' + this.config.tabs[i].name).removeClass(
								'active');
						$('div#' + this.config.tabs[i].name).removeClass(
								'active');
					}
					$('li.' + tabName).addClass('active');
					$('div#' + tabName).addClass('active');
				},

				onEditBean : function() {
					// edit bean
				},
				onClosePopupBean : function() {
					UIPopup.closePopup();
				},
				onClearBean : function() {
					// dien moi bean
				},
				onSaveBean : function() {
					this.config.action[0].save(this);
				},
				onRefreshBean : function() {
					// xem lai beans
				},
				onDeleteBean : function() {
					// xoa bean
				},
				onToggleBean : function(evt) {
				},
				onEditAction : function(evt) {

				},
				onExitBean : function(evt) {
					UIPopup.closePopup();
				},
				onToggleBean : function(evt) {
					var beanName = $(evt.target).closest("a").attr('bean');
					this._toggleBean(beanName);
					this.beanStates[beanName].select = true;
					this.render();
				},
				onUpdateBusiness: function(entity, fieldName){
					entity['fullName.vn']=$('[name="fullNameVN"]').val();
					entity.registrationCode=$('[name="registrationCodeD"]').val();
					entity['fullName.en']=$('[name="fullNameEN"]').val();
					entity.taxRegistrationCode=$('[name="taxRegistrationCode"]').val();
					entity.representative=$('[name="representativeD"]').val();
					entity.charterCapital=$('[name="charterCapital"]').val();
					entity.domain=$('[name="domain"]').val();
					entity.legalCapital=$('[name="legalCapital"]').val();
					entity.description=$('[name="descriptionDK"]').val();
						
					var tab= this.onFindTabByFieldAction(fieldName);
						//clear input
						for(var i=0; i<this.config1.fields.length; i++){
							if(this.config1.fields[i].numberOfTab === tab){
								$('[name="'+this.config1.fields[i].field+'"]').val('');
							}
						}
				},
				onUpdateContact: function(entity, fieldName){
					
					var phone=$('[name="phoneC"]').val().split(",");
					var mobile=$('[name="mobileC"]').val().split(",");
					var email=$('[name="emailC"]').val().split(",");
					var website=$('[name="website"]').val().split(",");
					var fax=$('[name="faxC"]').val().split(",");
					
					entity.addressNumber=$('[name="addressNumber"]').val();
					entity.street=$('[name="street"]').val();
					entity.district=$('[name="district"]').val();
					entity.country=$('[name="country"]').val();
					entity.city=$('[name="city"]').val();
					entity.phone=phone;
					entity.mobile= mobile;
					entity.fax = fax;
					entity.email=email;
					entity.website=website;
					//clear input
					var tab= this.onFindTabByFieldAction(fieldName);
					for(var i=0; i<this.config1.fields.length; i++){
						if(this.config1.fields[i].numberOfTab === tab)
						$('[name="'+this.config1.fields[i].field+'"]').val('');
					}	
				},
				onClickEditBusiness: function(evt){
					var numberOfArray= $('#businesstId').attr('value');
					//console.log(numberOfArray);
					$('[name="fullNameVN"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray]['fullName.vn']);
					$('[name="registrationCodeD"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray].registrationCode);
					$('[name="fullNameEN"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray]['fullName.en']);
					$('[name="taxRegistrationCode"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray].taxRegistrationCode);
					$('[name="representativeD"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray].representative);
					$('[name="charterCapital"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray].charterCapital);
					$('[name="domain"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray].domain);
					$('[name="legalCapital"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray].legalCapital);
					$('[name="descriptionDK"]').val(this.beanStates[this.name].bean[0].profiles.businessRegistrations[numberOfArray].description);
				},
				onClickEditTrContact: function(evt){
					//contactId
					var numberOfArray= $('#contactId').attr('value');
					//chuẩn bị sửa ở đây
					$('[name="addressNumber"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].addressNumber);
					$('[name="street"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].street);
					$('[name="district"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].district);
					$('[name="country"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].country);
					$('[name="city"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].city);
					$('[name="phoneC"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].phone);
					$('[name="mobileC"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].mobile);
					$('[name="faxC"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].fax);
					$('[name="emailC"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].email);
					$('[name="website"]').val(this.beanStates[this.name].bean[0].contacts[numberOfArray].website);
				},
			});

	return UIBean;
});