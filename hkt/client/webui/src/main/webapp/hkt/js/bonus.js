/*
  created by Van Thanh
 */
var moduleOfNavigation= null;
var menuIdClick= null;


var numberToggle = 0;
function onResize() {
	console.log('ngang '+ window.innerWidth);
	console.log('cao ' + window.innerHeight);
	if (window.innerWidth < 480 && window.innerWidth >= 321){
			showDienThoai();
			showDienThoaiPopup();
			showDienThoaiDocThongTinDoanhNghiep();
			showThongKeDienThoai();
		}
	else if(window.innerWidth < 600 && window.innerWidth >= 480){
			showDienThoaiNgang();
			showDienThoaiNgangPopup();
			showDienThoaiNgangThongTinDoanhNghiep();
			showThongKeDienThoai();
		}
	else if (window.innerWidth < 800 && window.innerWidth >= 600)
		{
			showMayTinhBangChieuDoc();
			showMayTinhBangChieuDocPopup();
			showMayTinhBangChieuDocThongTinDoanhNghiep();
		}
	else if(window.innerWidth < 1025 && window.innerWidth >= 800){
			showMayTinhBangChieuNgang();
			showMayTinhBangChieuNgangPopup();
			showMayTinhBangChieuNgangThongTinhDoanhNghiep();
			
		}
	else if(window.innerWidth >= 1025 && window.innerWidth < 1200){
			showDesktop();
			showDesktopThongTinhDoanhNghiep();
			showDesktopThongTinhDoanhNghiep();
		}
	else if(window.innerWidth >= 1200){
		showFullManHinh();
		}
}
//hiển thị thống kê điện thoại
function showThongKeDienThoai(){
	$('.nothing1Popup h4').css({display: 'none'});
	$('.nothing2Popup').css({top: '0px', left: '0px', bottom: '0px', right: '0px'});
	$('.nothing2Popup img').css({display: 'none'});
	$('.nothing3Popup').css({top: '0px', left: '0px', right: '0px', height: '91.5%'});
	$('div.actionControlgroupPopup ').css({paddingRight: '20px'});
	if(window.innerHeight < 300){
		$('#PopupPanelFullScreen').css({height: '300px', bottom: 'auto',overflowY: 'scroll'});
	}
	else{
		$('#PopupPanelFullScreen').css({height: '650px', bottom: 'auto',overflowY: 'scroll'});
	}
}
function showDienThoaiDocThongTinDoanhNghiep(){
	$('.thongTinDoanhNghiep1 div.col-xs-11').css({paddingRight: '15px', float: 'right', marginBottom: '1px'});
	$('.thongTinDoanhNghiep div.imageDoanhNghiep').css({width: '100%', marginRight: '3px', paddingRight: '0px', paddingLeft: '5px', height: '50px'});
	$('.thongTinDoanhNghiep1 div.col-xs-4').css({width: '30%'});
	$('.thongTinDoanhNghiep1 div.col-xs-8').css({width: '70%', height: '16px', paddingRight: '15px'});
	$('.thongTinDoanhNghiep1 div.col-md-4').css({marginTop: '5px'});
	$('.thongTinDoanhNghiep1 label').css({fontSize: '9px !important'});
	$('.thongTinDoanhNghiep1 textarea').css({float: 'right', width: '100%', fontSize: '9px', height: '20px'});
	$('.thongTinDoanhNghiep1 div.col-xs-6').css({width: '100%'});
	$('.thongTinDoanhNghiep1 div.middleBean').css({width: '40px',  overflow: 'hidden', textOverflow: 'ellipsis'});
	$('.thongTinDoanhNghiep1 input.onChangeInput').css({fontSize: '9px'});
	$('.thongTinDoanhNghiep1 div.col-xs-1').css({width: '30%'});
	$('.thongTinDoanhNghiep1 div.col-xs-11').css({width: '70%', paddingLeft: '0px'});
}
function showDienThoaiNgangThongTinDoanhNghiep(){
	$('.thongTinDoanhNghiep1 div.col-xs-11').css({paddingRight: '15px', float: 'right'});
	$('.thongTinDoanhNghiep div.imageDoanhNghiep').css({width: '44.6%', marginRight: '15px', paddingRight: '0px', paddingLeft: '5px', height: '80px'});
	$('.thongTinDoanhNghiep1 div.col-xs-4').css({width: '30%'});
	$('.thongTinDoanhNghiep1 div.col-xs-8').css({width: '70%', paddingRight: '15px', fontSize: '14px', height: '30px'});
	$('.thongTinDoanhNghiep1 div.col-md-4').css({marginTop: '5px'});
	$('.thongTinDoanhNghiep1 label').css({fontSize: '9px !important'});
	$('.thongTinDoanhNghiep1 textarea').css({float: 'right', width: '93%', height: 'auto', fontSize: '14px'});
	$('.thongTinDoanhNghiep1 div.col-xs-6').css({width: '50%'});
	$('.thongTinDoanhNghiep1 div.middleBean').css({width: '40px', overflow: 'hidden', textOverflow: 'ellipsis'});
	$('.thongTinDoanhNghiep1 input.onChangeInput').css({fontSize: '14px', width: '100%'});
	$('.thongTinDoanhNghiep1 div.col-xs-1').css({width: '8.33333333%'});
	$('.thongTinDoanhNghiep1 div.col-xs-11').css({width: '91.66666667%', marginBottom: '10px', paddingLeft: '0px'});
	
	$('').css({});
}
function showMayTinhBangChieuDocThongTinDoanhNghiep(){
	$('.thongTinDoanhNghiep1 div.col-xs-11').css({paddingRight: '10px', float: 'right'});
	$('.thongTinDoanhNghiep div.imageDoanhNghiep').css({width: '46.6%', marginRight: '15px', paddingRight: '0px', paddingLeft: '5px', height: '80px'});
	$('.thongTinDoanhNghiep1 input.onChangeInput').css({width: '100%', fontSize: '11px'});
	$('.thongTinDoanhNghiep1 div.col-xs-4').css({width: '30%'});
	$('.thongTinDoanhNghiep1 div.col-xs-8').css({width: '70%',  paddingRight: '0px', fontSize: '14px', height: '30px'});
	$('.thongTinDoanhNghiep1 div.col-md-4').css({marginTop: '5px'});
	$('.thongTinDoanhNghiep1 label').css({fontSize: '11px !important'});
	$('.thongTinDoanhNghiep1 textarea').css({float: 'right', width: '93.8%'});
	$('.thongTinDoanhNghiep1 div.col-xs-6').css({width: '50%'});
	$('.thongTinDoanhNghiep1 div.col-xs-1').css({width: '8.33333333%'});
	$('.thongTinDoanhNghiep1 div.col-xs-11').css({width: '91.66666667%', marginBottom: '10px', paddingLeft: '0px'});
}
function showMayTinhBangChieuNgangThongTinhDoanhNghiep(){
	$('.thongTinDoanhNghiep div.imageDoanhNghiep').css({width: '47.6%', marginRight: '15px', paddingRight: '0px', paddingLeft: '5px', height: '80px'});
	$('.thongTinDoanhNghiep1 input.onChangeInput').css({width: '100%', fontSize: '11px'});
	$('.thongTinDoanhNghiep1 div.col-xs-4').css({width: '20%'});
	$('.thongTinDoanhNghiep1 div.col-xs-8').css({width: '80%', paddingRight: '0px', fontSize: '14px', height: '30px'});
	$('.thongTinDoanhNghiep1 div.col-md-4').css({marginTop: '5px'});
	$('.thongTinDoanhNghiep1 label').css({fontSize: '14px !important'});
	$('.thongTinDoanhNghiep1 textarea').css({float: 'right', width: '96.4%'});
	$('.thongTinDoanhNghiep1 div.col-xs-6').css({width: '50%'});
	$('.thongTinDoanhNghiep1 div.col-xs-1').css({width: '8.33333333%'});
	$('.thongTinDoanhNghiep1 div.col-xs-11').css({width: '91.66666667%', marginBottom: '10px', paddingRight: '15px', paddingLeft: '0px'});
	$('.thongTinDoanhNghiep1 div.middleBean').css({width: 'auto', overflow: 'inherit', textOverflow: 'inherit'});
}
function showDesktopThongTinhDoanhNghiep(){
	$('.thongTinDoanhNghiep div.imageDoanhNghiep').css({width: '48%', marginRight: '15px', paddingRight: '0px', paddingLeft: '5px', height: '80px'});
	$('.thongTinDoanhNghiep1 input.onChangeInput').css({width: '100%', fontSize: '14px'});
	$('.thongTinDoanhNghiep1 div.col-xs-4').css({width: '17%'});
	$('.thongTinDoanhNghiep1 div.col-xs-8').css({width: '83%', paddingRight: '0px', fontSize: '14px', height: '30px'});
	$('.thongTinDoanhNghiep1 div.col-md-4').css({marginTop: '5px'});
	$('.thongTinDoanhNghiep1 textarea').css({float: 'right', width: '99%', height: 'auto', fontSize: '14px'});
	$('.thongTinDoanhNghiep1 div.col-xs-6').css({width: '50%', paddingRight: '15px', paddingLeft: '10px'});
	$('.thongTinDoanhNghiep1 div.col-xs-1').css({width: '8.33333333%'});
	$('.thongTinDoanhNghiep1 div.col-xs-11').css({width: '91.66666667%', marginBottom: '10px', paddingLeft: '10px', });
	$('.thongTinDoanhNghiep1 div.middleBean').css({width: 'auto', overflow: 'inherit', textOverflow: 'inherit'});
}

// Start popup
function showMayTinhBangChieuNgangPopup(){
	$('#PopupPanel').css({width: '723px', marginLeft: '50px'});
	$('.formBean').css({width: '50%', paddingLeft: '0px', paddingRight: '15px'});
	$('.col-xs-4').css({width: '30% !important'});
	$('.col-xs-8').css({width: '70% !important'});
	$('.middleBean').css({width: '30%'});
	$('.rowBean').css({marginLeft: '10px !important', paddingLeft: '10px'});
	$('.col-xs-8').css({width: '70% !important'});
	$('.tableBean').css({paddingLeft: '0px', marginLeft: '0px', paddingRight: '10px'});
	$('.col-md-2, .col-md-10').css({paddingLeft: '0px', paddingRight: '5px'});
	$('.col-xs-2').css({width: '16.66666667%'});
	$('.col-xs-10').css({width: '83.33333333%'});
	$('.btnBean').css({paddingLeft: '5px', paddingRight: '5px'});
	$('.codeBean').css({width: '80px', overflow: 'hidden' , textOverflow: 'ellipsis'});
	$('.tdBeans').css({width: '20% !important'});
	$('.borderTable').css({minHeight: '320px'});
	$('.actionControlgroup button').css({width: '16%', textAlign: 'center', color: 'black', fontSize: '14px', padding: '3px 3px 3px 3px'});
	$('.nothing2').css({paddingBottom: '45px'});
	$('.actionControlgroup button img').css({display: 'inherit', marginRight: '5px', height: '20px'});
	$('	.middleBean label, .tdBeans strong').css({fontSize: '14px !important'});
	$('.actionControlgroup').css({textAlign: 'right', width: '656px'});
	$('.inputPopup').css({paddingLeft: '13px'});
	$('.btnBean img').css({width: '40px', height: '40px'});
}


function showMayTinhBangChieuDocPopup(){
	$('#PopupPanel').css({width: '100%', marginLeft: '0px'});
	$('.formBean').css({width: '100%', paddingLeft: '0px', paddingRight: '10px'});
	$('.rowBean').css({marginLeft: '10px !important', paddingLeft: '10px'});
	$('.middleBean').css({width: '20%'});
	$('.col-xs-8').css({width: '80% !important'});
	$('.tableBean').css({paddingLeft: '0px', marginLeft: '0px', paddingRight: '10px'});
	$('.col-md-2, .col-md-12, .col-md-10').css({paddingLeft: '0px'});
	$('.col-xs-10, .col-xs-2').css({width: '100%'});
	$('.col-md-2, .col-md-10').css({paddingRight: '0px'});
	$('.btnBean').css({width: '20% !important', float: 'left', paddingLeft: '5px', paddingRight: '5px'});
	$('.codeBean').css({width: '90px', overflow: 'hidden' , textOverflow: 'ellipsis'});
	$('.tdBeans').css({width: '20% !important'});
	$('.borderTable').css({minHeight: '150px'});
	$('.actionControlgroup button').css({width: '32%', textAlign: 'center', color: 'black', fontSize: '13px', padding: '3px 3px 3px 3px'});
	$('.nothing2').css({paddingBottom: '70px'});
	$('.actionControlgroup button img').css({display: 'inherit', marginRight: '5px', height: '20px'});
	$('	.middleBean label, .tdBeans strong').css({fontSize: '13px !important'});
	$('.actionControlgroup').css({textAlign: 'left', width:'86%'});
	$('.btnBean img').css({width: '40px', height: '40px'});
}

function showDienThoaiNgangPopup(){
	$('#PopupPanel').css({width: '100%', marginLeft: '0px'});
	$('.formBean').css({width: '100%', paddingLeft: '0px', paddingRight: '0px'});
	$('.rowBean').css({marginLeft: '0px !important', paddingLeft: '10px'});
	$('.middleBean').css({width: '25%'});
	$('.col-xs-8').css({width: '75% !important'});
	$('.tableBean').css({paddingLeft: '0px', marginLeft: '0px', paddingRight: '0px'});
	$('.col-md-2, .col-md-12, .col-md-10').css({paddingLeft: '0px'});
	$('.col-xs-10, .col-xs-2').css({width: '100%'});
	$('.col-md-2, .col-md-10').css({paddingRight: '0px'});
	$('.btnBean').css({width: '20% !important', float: 'left', paddingLeft: '5px', paddingRight: '5px'});
	$('.codeBean').css({width: '70px', overflow: 'hidden' , textOverflow: 'ellipsis'});
	$('.tdBeans').css({width: '20% !important'});
	$('.borderTable').css({minHeight: '150px'});
	$('.actionControlgroup button').css({width: '32%', textAlign: 'center', color: 'black', fontSize: '12px', padding: '3px 3px 3px 3px'});
	$('.nothing2').css({paddingBottom: '70px'});
	$('.actionControlgroup button img').css({display: 'inherit', marginRight: '5px', height: '20px'});
	$('	.middleBean label, .tdBeans strong').css({fontSize: '12px !important'});
	$('.actionControlgroup').css({width: '86%', textAlign: 'left'});
	$('.btnBean img').css({width: '40px', height: '40px'});
}
function showDienThoaiPopup(){
	$('#PopupPanel').css({width: '100%', marginLeft: '0px'});
	$('.formBean').css({width: '100%', paddingLeft: '0px', paddingRight: '0px'});
	$('.rowBean').css({marginLeft: '0px !important', paddingLeft: '10px'});
	$('.middleBean').css({width: '25%'});
	$('.col-xs-8').css({width: '75% !important'});
	$('.tableBean').css({paddingLeft: '0px', marginLeft: '0px', paddingRight: '0px'});
	$('.col-md-2, .col-md-12, .col-md-10').css({paddingLeft: '0px'});
	$('.col-xs-10, .col-xs-2').css({width: '100%'});
	$('.col-md-2, .col-md-10').css({paddingRight: '0px'});
	$('.btnBean').css({width: '20% !important', float: 'left', paddingLeft: '5px', paddingRight: '5px'});
	$('.codeBean').css({width: '31px', overflow: 'hidden' , textOverflow: 'ellipsis'});
	$('.tdBeans').css({width: '20% !important'});
	$('.borderTable').css({minHeight: '150px'});
	$('.actionControlgroup button').css({width: '31%', textAlign: 'center', color: 'black', fontSize: '10px', padding: '3px 3px 3px 3px'});
	$('.nothing2').css({paddingBottom: '70px'});
	$('.actionControlgroup button img').css({display: 'none'});
	$('	.middleBean label, .tdBeans strong').css({fontSize: '10px !important'});
	$('.actionControlgroup').css({width: '80%', textAlign: 'left'});
	$('.btnBean img').css({width: '20px', height: '20px'});
}


//=================Giao diện chính======================================================================

function showDienThoai() {
	//console.log("dang o dien thoai nhe");
	$('#navspace').css({display: 'none'});
	$('#quangCao1, #quangCao').css({display: 'none'});
	$('#menu').css({display: 'block'});
	$('.col-xs-6').css({width: '33.33%'});
	$('.menuCol').css({padding: '0px 0px 0px 13px',  height: '83px'});
	$('.menuCol img').css({height: '43%', width: '30%',  position: 'relative', top: '50%', transform: 'translateY(-50%)'});
	$('.menuCol a').css({padding: '6px 3px 6px 3px', float: 'left', width:'68%', fontSize: 'smaller', position: 'relative', top: '50%', transform: 'translateY(-50%)', color: 'black'});
	$('fieldset').css({paddingLeft: '5px !important', left: '0px', marginLeft:' 2px !important'});
	$('.menuModule ul').css({ width: '70%'});
	$('#rowIcon').css({ left: '4%', right: '0%'});
	$('.menuModule li').css({ fontSize: 'inherit'});
	$('#BannerView').css({ right: '1.2%', left: '0.8%'});
	$('.menuBannerView').css({display: 'none'});
}
function showDienThoaiNgang()
{
	//console.log('dang o chieu ngang dien thoai');
	//$("#navspace").fadeOut(100);
	$('#navspace').css({display: 'none'});
	$('#quangCao1, #quangCao').css({display: 'none'});
	$('#menu').css({display: 'block'});
	$('.col-xs-6').css({width: '33.33%'});
	$('.menuCol').css({padding: '0px 0px 0px 13px',  height: '90px'});
	$('.menuCol img').css({height: '45%', width: '30%',  position: 'relative', top: '50%', transform: 'translateY(-50%)'});
	$('.menuCol a').css({padding: '6px 3px 6px 8px', float: 'left', width:'70%', fontSize: 'small', position: 'relative', top: '50%', transform: 'translateY(-50%)', color: 'black'});
	$('fieldset').css({paddingLeft: '5px !important', left: '0px', marginLeft:' 2px !important'});
	$('.menuModule ul').css({ width: '40%'});
	$('#rowIcon').css({ left: '4%', right: '0%'});
	$('.menuModule li').css({ fontSize: 'inherit'});
	$('#BannerView').css({ right: '1.2%', left: '0.8%'});
	$('.menuBannerView').css({display: 'none'});
}

function showMayTinhBangChieuDoc()
{
	//console.log('dang o may tinh bang chieu doc');
	document.getElementById('navspace').style.display = 'block';
	$('#quangCao1, #quangCao').css({display: 'none'});
	$('#menu').css({display: 'none'});
	$('.row').css({paddingLeft: '0px'});
	$('.col-xs-6').css({width: '33.33%'});
	$('.menuCol').css({padding: '0px 0px 0px 13px',  height: '90px'});
	$('.menuCol img').css({height: '30%', width: '30%',  position: 'relative', top: '50%', transform: 'translateY(-50%)'});
	$('.menuCol a').css({padding: '6px 3px 6px 8px', float: 'left', width:'70%', fontSize: 'smaller', position: 'relative', top: '50%', transform: 'translateY(-50%)', color: 'black'});
	$('fieldset').css({paddingLeft: '5px !important', left: '0px', marginLeft:' 2px !important'});
	$('.menuModule ul').css({ width: '42%'});
	$('#rowIcon').css({ left: '45%', right: '0%'});
	$('.menuModule li').css({ fontSize: 'inherit'});
	$('#BannerView').css({ right: '0.8%', left: '0%'});
	$('.menuBannerView').css({display: 'none'});
}

function showMayTinhBangChieuNgang()
{
	//console.log('dang o may tinh bang chieu ngang');
	document.getElementById('navspace').style.display = 'block';
	$('#quangCao1').css({ width: '15%', display: 'block'});
	$('#quangCao').css({display: 'none'});
	$('#menu').css({display: 'none'});
	$('.row').css({paddingLeft: '0px'});
	//$('.col-xs-6').css({width: '33.33%'});
	$('.menuCol').css({padding: '0px 0px 0px 13px',  height: '95px'});
	$('.menuCol img').css({height: '40%', width: '30%',  position: 'relative', top: '50%', transform: 'translateY(-50%)'});
	$('.menuCol a').css({padding: '6px 3px 6px 8px', float: 'left', width:'70%', fontSize: 'small', position: 'relative', top: '50%', transform: 'translateY(-50%)', color: 'black'});
	$('fieldset').css({paddingLeft: '5px !important', left: '0px', marginLeft:' 2px !important'});
	$('.menuModule ul').css({ width: '33%'});
	$('#rowIcon').css({ left: '50%', right: '0%'});
	$('.menuModule li').css({ fontSize: 'larger'});
	$('#BannerView').css({ right: '0.8%', left: '15%'});
	$('.menuBannerView').css({display: 'none'});
}

function showDesktop()
{
	//console.log('dang o desktop');
	document.getElementById('navspace').style.display = 'block';
	$('#quangCao1, #quangCao').css({ width: '15%', display: 'block'});
	$('#menu').css({display: 'none'});
	$('.row').css({paddingLeft: '0px'});
	//$('.col-xs-6').css({width: '33.33%'});
	$('.menuCol').css({padding: '0px 0px 0px 13px',  height: '95px'});
	$('.menuCol img').css({height: 'auto', width: 'auto',  position: 'relative', top: '50%', transform: 'translateY(-50%)'});
	$('.menuCol a').css({padding: '6px 3px 6px 13px', float: 'left', width:'55%', fontSize: 'small', position: 'relative', top: '50%', transform: 'translateY(-50%)', color: 'black'});
	$('fieldset').css({paddingLeft: '5px !important', left: '0px', marginLeft:' 2px !important'});
	$('.menuModule ul').css({ width: '26%'});
	$('#rowIcon').css({ left: '42.8%', right: '15%'});
	$('.menuModule li').css({ fontSize: 'larger'});
	$('#BannerView').css({ right: '15.6%', left: '15%'});
	$('.menuBannerView a').css({width: '62%'});
	$('.menuBannerView').css({display: 'block'});
	$('.menuBannerView div').css({width: '11.3%'});
}
function showFullManHinh(){
	document.getElementById('navspace').style.display = 'block';
	$('#quangCao1, #quangCao').css({ width: '17.5%', display: 'block'});
	$('#quangCao').css({ width: '17.5%', display: 'block'});
	$('.row').css({paddingLeft: '0px'});
	$('#menu').css({display: 'none'});
	//$('.col-xs-6').css({width: '33.33%'});
	$('.menuCol').css({padding: '0px 0px 0px 13px',  height: '95px'});
	$('.menuCol img').css({height: 'auto', width: 'auto',  position: 'relative', top: '50%', transform: 'translateY(-50%)'});
	$('.menuCol a').css({padding: '6px 3px 6px 13px', float: 'left', width:'52%', fontSize: 'small', position: 'relative', top: '50%', transform: 'translateY(-50%)', color: 'black'});
/*	$('fieldset').css({paddingLeft: '5px !important', left: '0px', position: 'absolute', marginLeft:' 2px !important'});
*/	$('.menuModule ul').css({ width: '21.7%'});
	$('#rowIcon').css({ left: '40.5%', right: '17.5%'});
	$('.menuModule li').css({ fontSize: 'larger'});
	$('#BannerView').css({ right: '17.91%', left: '17.50%'});
	$('.menuBannerView a').css({width: '49%'});
	$('.menuBannerView').css({display: 'block'});
	$('.menuBannerView div').css({width: '12%'});
} 
//===========================END Giao diện chính===============================================
function onOverMenu() {
	if (numberToggle == 0) {
		document.getElementById('testMenu').innerHTML = 'Ẩn menu';
		$('#navspace').fadeIn(100);
		$('#doanhNghiep').fadeOut(100);
		numberToggle = 1;
	} else if (numberToggle == 1) {
		$('#navspace').fadeOut(100);
		document.getElementById('navspace').style.display = 'block';
		numberToggle = 0;
		document.getElementById('testMenu').innerHTML = 'Mở menu';
	}
}
