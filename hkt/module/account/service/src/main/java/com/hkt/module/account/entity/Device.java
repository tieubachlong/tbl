package com.hkt.module.account.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.hkt.module.core.entity.AbstractPersistable;

@Entity
@Table
public class Device extends AbstractPersistable<Long> {
	private static final long serialVersionUID = 1L;

	private String deviceId;
	private String namePC;

	public String getNamePC() {
		return namePC;
	}

	public void setNamePC(String namePC) {
		this.namePC = namePC;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	@Override
	public String toString() {
		return deviceId;
	}
}
