package com.hkt.module.account.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.hkt.module.account.entity.Device;

public interface DeviceRepository extends CrudRepository<Device, Long> {

	Device getByDeviceId(String deviceId);
	
	@Modifying
	@Query("DELETE FROM Device WHERE deviceId = :deviceId")
	public void deleteByDeviceId(@Param("deviceId") String deviceId);
	
}